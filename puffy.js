function clearInnerById(val){
	var element = document.getElementById(val);
	if (element){
		element.innerHTML = '_';
	}
}
function pressCell(info){
	var currentTd = info.target,
		insert = document.getElementById('insert'),
		button = document.getElementById('button');
		insert.value = currentTd.id;
		button.click();
}
function createTable(size){
	var table = document.createElement('table');
	table.setAttribute('border','1');
	table.setAttribute('cellspacing','0');
	table.setAttribute('id','table');
	for (let i = size - 1; i >= 0; i--){
		let tr = document.createElement('tr');
		for (let j = 0; j < size; j++){
			let td = document.createElement('td');
			td.setAttribute('id', String(j) + i);
			td.onclick = pressCell;
			tr.appendChild(td);
		}
		table.appendChild(tr);
	}
	var div = document.getElementById('map');
	div.appendChild(table);
	for ( let i = 0; i < size; i++){
		let elementX = document.getElementById('0'+ i);
		elementX.innerHTML = i;
		elementX.setAttribute('class', 'number');
		let elementY = document.getElementById(i + '0');
		elementY.innerHTML = i;
		elementY.setAttribute('class', 'number');
	}
}
class CatZone {
	constructor(strength, orientation, x, y){
		this.str         = strength;
		this.currentStr  = strength;
		this.orientation = orientation;
		this.x = x;
		this.y = y;
		console.log('Created catZone: '+strength+'.');
	}
	get coordinates(){
		return [this.x, this.y];
	}
	getTouch(val = 1){
		this.currentStr = this.currentStr - val;
		return this.currentStr;
	}
	strength(current = true){
		if (current){
			return this.currentStr;
		} else {
			return this.str;
		}
	}
}
class Observer {
	constructor(target = null){
		this.target = target;
		this.owner   = null;
	}
	update(){
		if (this.owner == null || typeof(this.owner) != 'object' ){
			console.log(this.constructor.name + ' do not have any owner.');
			return false;
		}
	}
	setOwner(owner){
		this.owner = owner;
	}
}
class Table extends Observer {
	update(){
		super.update();
		var map  = this.owner.map,
			size = this.owner.size,
			element = null,
			_class = '';
		if ( map == null ){
			return false;
		}
		for (var i = 0; i < size; i++) {
			for (var j = 0; j < size; j++) {
				if ( ! (element = document.getElementById(String(j)+i)) ){
					return false;
				}
				if ( map[i][j] == 0 ){
					//element.setAttribute('class', 'miss');
					_class = 'miss';
				} else if ( map[i][j] == 1 ){
					//element.setAttribute('class', 'clear');
					_class = 'clear';
				} else if ( map[i][j] == 2 ){
					//element.setAttribute('class', 'around');
					_class = 'around';
				} 
				if ( _class ){
					let oldClass = element.getAttribute('class');
					if ( oldClass != null ){
						if ( oldClass.indexOf(_class) == -1 ){
							if ( oldClass.indexOf('chit') == -1 ){
								_class = _class + ' ' + oldClass;
							}
							element.setAttribute('class', _class);
							if ( oldClass.indexOf('number') >= 0 ){
								element.setAttribute('style', 'color: black');
							}
						}
					} else {
						element.setAttribute('class', _class);
					}
					_class = '';
				}
			} 
		}
	}
}
class Message extends Observer {
	update(){
		super.update();
		var element = document.getElementById(this.target);
		if (element){
			element.innerHTML = this.owner.message;
			if ( ! this.owner.interruptStat ){
				setTimeout(clearInnerById, 5000, this.target);
			}
		}
	}
}
class Map {
	constructor(size){
		this.czq       = 0;
		this.size      = size;
		this.map       = new Array(size);
		this.message   = '';
		this.updateAccess = false;
		this.observers    = null;
		this.interruptStat= false;
		for (var i = 0; i < size; i++) {
			this.map[i] = new Array(size);
			for (var j = 0; j < size; j++) {
				this.map[i][j] = null;
			} 
		}
		console.log('Created map: '+ size+ 'x' + size+'.');
	}
	interrupt(){
		this.interruptStat = true;
		this.map = null;
		document.getElementById('table').remove();
		changeForm();
		console.log('Игра остановлена по причине отсутствия зон.');
		this.setMessage('Поздравляем, котик доволен лаской... а теперь неси жрать.');
		this.updateAccess = true;
		this.update();
	}
	setObserver(obj){
		if ( ! (obj instanceof Observer) ){
			console.log(obj.constructor.name + ' not observer.');
			return false;
		}
		obj.setOwner(this);
		if ( this.observers == null ){
			this.observers = new Array(1);
			this.observers[0] = obj;
		} else {
			this.observers.push(obj);
		}
	}
	update(){
		if (this.observers == null){
			return false;
		}
		if ( ! this.updateAccess ){
			return false;
		}
		for (var i = 0; i < this.observers.length; i++) {
			if ( this.observers[i] != null ){
				this.observers[i].update();
			}
		}
		this.updateAccess = false;
	}
	setMessage(string){
		this.message = string;
		this.update();
	}
	valid(x, y){
		if (x < this.size && x >= 0 &&
			y < this.size && y >= 0){
			return true;
		}
		return false;
	}
	setZone(str){
		this.czq++;
		var x = Math.floor( Math.random()*this.size ),
			y = Math.floor( Math.random()*this.size ),
			orientation = ( Math.random() >= 0.5 ),
			localStep = this.size*this.size,
			x2 = 0, y2 = 0, accept = true;

		console.log('Попытка размещения зоны (длиной '+str+'): '+ x + ' '+ y);
		for (var i = 0; i < 10; i++){
			console.log('Поиск координат');
			for ( var j = 0; j <= localStep; j++){
				if ( this.map[y][x] == null ){
					break;
				} else {
					x = Math.floor( Math.random()*this.size );
					y = Math.floor( Math.random()*this.size );
				}
			}
			if ( this.map[y][x] == null ){
				console.log('Найдены координаты, удовлетворяющие критериям поиска: '+ x + ', '+ y);
			} else {
				console.log('Координаты не найдены. Попытка № '+ (i + 1) );
				continue;
			}
			for ( var k = 0; k < 2; k++){
				accept = true;
				console.log( (orientation ? 'Горизонтальное':'Вертикальное') + ' размещение.');	
				if ( str != 1) {
					if ( orientation ){ // --
						x2 = x - str + 1; y2 = y;
						for (; x2 <= x; x2++){
							console.log('Проверка зоны: '+x2+'-'+(x2+str-1)+', '+y2);
							accept = true;
							for ( var j = 0; j < str; j++){
								if ( ! this.valid(y2, x2 + j) || this.map[y2][x2 + j] != null ){
									accept = false;
									break;
								}
							}
							if ( ! accept ){
								console.log('Зона не удовлетворяет критериям поиска');
								accept = false;
								continue;
							}
							console.log('Зона удовлетворяет критериям поиска. Проверка облости.');
							if ( this.valid(y2, x2 - 1)   && this.map[y2][x2 -1]    != null ||
								 this.valid(y2, x2 + str) && this.map[y2][x2 + str] != null ){
								console.log('Края области заняты.');
								accept = false;
								continue;
							}
							for ( j = -1; j <= str; j++){
								if ( this.valid(y2 + 1, x2 + j) && this.map[y2 + 1][x2 + j] != null ||
									 this.valid(y2 - 1, x2 + j) && this.map[y2 - 1][x2 + j] != null ){
									accept = false;
									console.log('Область занята');
									break;
								}
							}
							if ( accept ){
								break;
							}
						}
					} else {            // |
						x2 = x; y2 = y - str + 1;
						for (; y2 <= y; y2++){
							console.log('Проверка зоны: '+x2+', '+y2+'-'+(y2+str-1) );
							accept = true;
							for ( var j = 0; j < str; j++){
								if ( ! this.valid(y2 + j, x2) || this.map[y2 + j][x2] != null ){
									accept = false;
									break;
								}
							}
							if ( ! accept ){
								console.log('Зона не удовлетворяет критериям поиска');
								accept = false;
								continue;
							}
							console.log('Зона удовлетворяет критериям поиска. Проверка облости.');
							if ( this.valid(y2 - 1,   x2) && this.map[y2 -1][x2]    != null ||
								 this.valid(y2 + str, x2) && this.map[y2 + str][x2] != null ){
								console.log('Края области заняты.');
								accept = false;
								continue;
							}
							for ( j = -1; j <= str; j++){
								if ( this.valid(y2 + j, x2 + 1) && this.map[y2 + j][x2 + 1] != null ||
									 this.valid(y2 + j, x2 - 1) && this.map[y2 + j][x2 - 1] != null ){
									accept = false;
									console.log('Область занята');
									break;
								}
							}
							if ( accept ){
								break;
							}
						}
					}
				} else {
					let arrx = [-1,0,1,1,1,0,-1,-1],
						arry = [1,1,1,0,-1,-1,-1,0],
						cx = x,
						cy = y,
						add = -1,
						max = 0;
					for ( let m = 0; m < 2; m++){
						for (; cx > max; cx--){
							console.log('Проверка зоны: '+cx+', '+y);
							if ( this.map[y][cx] == null ){
								accept = true;
								for ( let z = 0; z < arrx.length; z++){
									if( this.valid( y+arry[z], cx+arrx[z] )    && 
										this.map[ y+arry[z] ][ cx+arrx[z] ] != null){
										console.log('Зона занята в точке: '+(cx+arrx[z])+', '+(y+arry[z]) );
										accept = false;
										break;
									}
								}
								if ( accept ){
									break;
								}
							}
						}
						if ( accept ){
							break;
						}
						max = x;
						cx = this.size - 1;
					}
					if ( accept ){
						x2 = cx;
						y2 = y;
						console.log('Уточнены координаты размещения: '+x2+', '+y2);
						break;
					}
					max = 0;
					for ( let m = 0; m < 2; m++){
						for (; cy > max; cy--){
							console.log('Проверка зоны: '+x+', '+cy);
							if ( this.map[cy][x] == null ){
								accept = true;
								for ( let z = 0; z < arrx.length; z++){
									if( this.valid( cy+arry[z], x+arrx[z] ) && 
										this.map[ cy+arry[z] ][ x+arrx[z] ] != null     ){
										accept = false;
										break;
									}
								}
								if ( accept ){
									break;
								}
							}
						}
						if ( accept ){
							break;
						}
						max = y;
						cy = this.size - 1;
					}
					if ( accept ){
						y2 = cy;
						x2 = x;
						console.log('Уточнены координаты размещения: '+x2+', '+y2);
						break;
					}
				}
				if ( accept ){
					break;
				} else if (str > 1){
					console.log('Смена ориентации.');
					orientation = !orientation;
				} else {
					break;
				}
			}
			if ( accept ){
				break;
			} else {
				x = 0;
				y = 0;
			}
		}
		if ( accept ){
			var cz = new CatZone(str, orientation, x2, y2);
			var td = null;
			for ( var k = 0; k < str; k++){
				if (orientation){
					this.map[y2][x2 + k] = cz;
					td = document.getElementById( String(x2 + k) + y2 );
				} else {
					this.map[y2 + k][x2] = cz;
					td = document.getElementById( String(x2) + (y2 + k) );
				}
				let oldClass = td.getAttribute('class');
				if ( oldClass != null ){
					oldClass+= ' chit';
				} else {
					oldClass = 'chit';
				}
				td.setAttribute('class', oldClass);
			}
			console.log('Размещение зоны ('+str+'): '+x2+', '+y2+'.Направление - '+ (orientation ? 'Горизонтальное':'Вертикальное') );
			return true;
		} else {
			console.log('Ошибка размещения зоны.');
			this.interrupt();
			return false;
		}
	}
	activate(x,y){
		let mess = 'Здесь не трожь! Цап-царап.';
		this.updateAccess = true;
		if ( ! this.valid(y, x) ){
			mess = 'Кот в другой стороне!';
		} else {
			if ( this.map[y][x] instanceof CatZone ){
				if ( ! this.map[y][x].getTouch() ){
					this.czq--;
					mess = 'Котееньке приятно! Аж замурчал!';
					let xy = this.map[y][x].coordinates,
					length = this.map[y][x].strength(false),
					arrx = null,
					arry = null;
					console.log('Начало зоны: '+ xy[0]+', '+ xy[1]);
					if ( this.map[y][x].orientation ){ // --
						arrx = [xy[0]-1,(xy[0]+length)];
						arry = [xy[1],xy[1]];
						for (let j = 0; j < 2; j++){
							for (let i = -1; i <= length; i++){
								arrx.push(xy[0]+i);
								if ( j ){
									arry.push(xy[1]-1);
								} else {
									arry.push(xy[1]+1);
								}
							}
						}
					} else {// |
						arrx = [xy[0],xy[0]];
						arry = [xy[1]-1,(xy[1]+length)];
						for (let j = 0; j < 2; j++){
							for (let i = -1; i <= length; i++){
								arry.push(xy[1]+i);
								if ( j ){
									arrx.push(xy[0]-1);
								} else {
									arrx.push(xy[0]+1);
								}
							}
						}
					}
					if ( arry != null && arrx != null && (arry.length == arrx.length) ){
						length = arry.length;
						let stringx = '', stringy = '';
						for (let i = 0; i < length; i++){
							stringx += arrx[i];
							stringy += arry[i];
						}
						console.log(stringx);
						console.log(stringy);
						for ( let i = 0; i < length; i++){
							if ( this.valid(arry[i], arrx[i]) ){
								if ( this.map[ arry[i] ][ arrx[i] ] == null ){
									this.map[ arry[i] ][ arrx[i] ] = 2;
								}
							}
						}
					} else {
						console.log('Ошибка зачистки зоны.');
					}
				} else {
					mess = 'Вы гладите в правильном направлении!';
				}
				this.map[y][x] = 1;
			} else if ( this.map[y][x] != null ) {
				this.updateAccess = false;
			} else {
				this.map[y][x] = 0;
			}
		}
		this.setMessage(mess);
		this.update();
		if ( ! this.czq ){
			this.interrupt();
		}
	}
	print(){
		if (this.map != null ){
			console.log('Map: ');
			var level = '';
			for( var i = 0; i < this.size; i++){
				for( var j = 0; j < this.size; j++){
					if ( this.map[i][j] == null ){
						level += 0;
					} else {
						level += 1;
					}
					level += ' ';
				}
				console.log(level);
				level = '';
			}
		}
	}
	reverse(){
		var newMap = new Array(this.size);
		for( var i = this.size - 1; i >= 0 ; i--){
			newMap[this.size - i - 1] = new Array(this.size);
			for( var j = 0; j < this.size; j++){
				newMap[this.size - i - 1][j] = this.map[i][j];
			}
		}
		this.map = newMap;
	}
	
}
function playerInsert(){
	var input = document.getElementById('insert');
	if ( input.value.length == 2 ){
		map.activate(input.value[0], input.value[1]);
		input.value = '';
		input.setAttribute('style','color:black');
	} else {
		input.setAttribute('style','color:red');
		console.log('Uncorrect coordinates.');
	}
}
//<form id="form">
//			<input type="text"   id="insert" placeholder="00">
//			<input type="button" id="button" value="Tickle!">
//<			input type="hidden" id="game"   value="NewGame">
//		</form>
function changeForm(){
	var textField = document.getElementById('insert'),
		button    = document.getElementById('button'),
		submit    = document.createElement('input');
	textField.setAttribute('type','hidden');
	button.setAttribute('type','hidden');
	submit.setAttribute('type','submit');
	submit.setAttribute('value','Новая игра');
	document.getElementById('form').appendChild(submit);
}
function init(){
	var button = document.getElementById('button');
	button.onclick = playerInsert;
	var insert = document.getElementById('insert');
	insert.onkeypress = function (key) {
		if (key.keyCode == 13){
			let button = document.getElementById('button');
			button.click();
			return false;
		}
	}
}
function game(){
	clearInnerById('mess');
	var size = 10;
	var zoneMax = 4;
	map = new Map(size); //global
	createTable(size);
	map.setObserver(new Table());
	map.setObserver(new Message('mess'));
	//map.setZone(2);
	//return;
	for (var i = zoneMax; i ; i--){
		for (var j = i; j <= zoneMax; j++){
			if ( ! map.setZone(i) ){
				i = 1;
				break;
			}
		}
	}
	//map.reverse();
	map.print();	
}

init();
game();

